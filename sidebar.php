<?php /* SIDEBAR */ ?>
<?php $sidebar = ''; ?>
<?php $sidebar = get_option('sidebar'); if ( $sidebar == '' ) $sidebar = 'sidebar-right'; else $sidebar = get_option('sidebar');?>

<div id="sidebar"<?php if ($sidebar == 'sidebar-left' ) echo ' class="sidebar-left"'; ?>>
	<div class="twtr-wdg">
				<script src="http://widgets.twimg.com/j/2/widget.js"></script>
				<script>
				new TWTR.Widget({
				  version: 2,
				  type: 'search',
				  search: 'from:holtrenfrew OR from:holtrenfrewPR',
				  interval: 30000,
				  title: '',
				  subject: 'TWITTER FEED',
				  width: 300,
				  height: 300,
				  theme: {
					shell: {
					  background: '#000',
					  color: '#FFF'
					},
					tweets: {
					  background: '#FFF',
					  color: '#000',
					  links: '#CC0066'
					}
				  },
				  features: {
					scrollbar: false,
					loop: true,
					live: true,
					behavior: 'default'
				  }
				}).render().start();
				</script>
	</div>
<div id="instagram-container" style="background-color:black; width:300px;height:310px">
<h3 style=" background: none repeat scroll 0 0 #000000;color: #FFFFFF; font-size: 13pt;line-height: 16pt;padding:7px 8px 8px 11px">Insta-Muse</h3>
<iframe src="http://snapwidget.com/in/?u=aG9sdHJlbmZyZXd8aW58ODB8M3wzfDAwMDAwMHxub3w0" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:252px; height: 252px;margin:2px 0px 25px 24px;" ></iframe>
</div>
	<?php if ( is_active_sidebar( 'general-up-widget-area' ) ) : ?>
        <ul>
            <?php dynamic_sidebar( 'general-up-widget-area' ); ?>
        </ul>
    <?php endif; ?>
	<?php 
	if ( is_single() || is_page() )
	{
		$sidebar =  get_post_meta($post->ID, "sidebars", true);
		$sidebar = str_replace(' ','-',strtolower($sidebar));
		if ($sidebar != '' )
		{
			if ( is_active_sidebar($sidebar) )
			{
				echo '<ul>';
					dynamic_sidebar($sidebar);
				echo '</ul>';
			}
		}
	}
	?>
	<?php if(is_front_page()): ?>
        <?php if ( is_active_sidebar( 'front-page-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'front-page-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?>    
    <?php if(is_single()): ?>
        <?php if ( is_active_sidebar( 'single-post-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'single-post-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?>   
    <?php if(is_page()): ?>
        <?php if ( is_active_sidebar( 'single-page-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'single-page-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?>     
    <?php if(is_category()): ?>
        <?php if ( is_active_sidebar( 'category-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'category-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?>     
    <?php if(is_archive()): ?>
        <?php if ( is_active_sidebar( 'archive-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'archive-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?> 
    <?php if(is_search()): ?>
        <?php if ( is_active_sidebar( 'search-widget-area' ) ) : ?>
            <ul>
                <?php dynamic_sidebar( 'search-widget-area' ); ?>
            </ul>
        <?php endif; ?>    
    <?php endif; ?>            
	<?php if ( is_active_sidebar( 'general-down-widget-area' ) ) : ?>
        <ul>
            <?php dynamic_sidebar( 'general-down-widget-area' ); ?>
        </ul>
    <?php endif; ?>    
<!--end of sidebar -->
</div>