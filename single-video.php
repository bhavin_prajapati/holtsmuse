				<?php $slider_display = get_option('slider_display'); if ( $slider_display == '' ) $slider_display = '1'; else $slider_display = get_option('slider_display'); ?>
                <?php if ( $slider_display == 0 ) : ?>             
                    <h1 class="title">
                        <?php the_title(); ?>
                        <!--end of entry title -->
                    </h1>
                <?php endif; ?>
                <?php if ( get_option('postmeta') == '' ) $postmeta = 'yes'; else $postmeta = get_option('postmeta'); ?>
                <?php if ( $postmeta == 'yes' ): ?>
                    <!--div class="post-meta">
           
                        <span class="date">Posted on: <?php the_time('jS'); echo ' '; the_time('F'); echo ' '; the_time('Y');?></span>
                                            
                   
                    </div--> <!-- end of post meta -->
					<div class="related-sep"></div>
                <?php endif; ?>
				<?php
					$video_url = get_post_meta($post->ID, "portfolio-video", true); 	
					echo do_shortcode('[duotive-video source="'. $video_url .'"]');
				?>
                <div class="entry-content">
                    <?php the_content('Read More'); ?>
                    <?php wp_link_pages( array( 'before' => '<span class="page-link">' . 'Pages:', 'after' => '</span>' ) ); ?>
                <!--end of entry content -->
				<p><?php the_tags(); ?></p>
                </div>
                <?php if ( get_the_author_meta( 'description' ) ) : ?>
                    <div id="author-info">
                        <div id="author-avatar">
							<?php $avatar_path = get_bloginfo('template_directory').'/images/default-avatar.png'; ?>
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), $size = '74', $default = $avatar_path ); ?>
                        <!--end of author avatar -->
                        </div>
                        <div id="author-description">
                            <h6><?php echo __('About ', 'duotive').get_the_author(); ?></h6>
                            <p><?php the_author_meta( 'description' ); ?></p>
                            <a class="more-url" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                            	<?php echo __('View all posts','duotive'); ?>
                            </a>                            
                        <!-- end of author description -->
                        </div>
                    <!-- end of author info -->
                    </div>
                <?php endif; ?>  
            <!--end of post -->