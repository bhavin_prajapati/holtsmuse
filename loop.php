<?php /* LOOP TO DISPLAY POSTS */ ?>

<?php // NO POSTS TO DISPLAY? ?>
<?php if ( ! have_posts() ) : ?>
	<p class="intro"><?php echo __('Apologies, but no results were found for the requested archive.<br /> Perhaps searching will help find a related post.','duotive'); ?>
<?php endif; ?>
<?php // HAVE POSTS TO DISPLAY? ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="blog-modern-wrapper">
        <div class="post-big-meta">
				<!--span class="post-date">
					<strong><?php the_time('j');?><em><?php the_time('S');?></em></strong>
					<small><?php the_time('F');?></small>
					<?php the_time('Y');?>
				</span-->
				<?php if ( $post->comment_status == 'open' ) : ?>
					<span class="sep"></span>
					<span class="post-comments">
						<?php comments_popup_link('0','1','%','title',''); ?>                                 
					</span>    
				<?php endif; ?>                   
		<!-- end of post big meta -->
		</div>
        <div class="post blog-modern">
            <?php if ( has_post_thumbnail() ): ?>
                <?php $thumbnail_src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>
                <a class="post-image" href="<?php the_permalink(); ?>" title="<?php the_title();?>">
                    <img src="<?php echo get_bloginfo('template_directory');?>/includes/timthumb.php?src=<?php echo $thumbnail_src; ?>&h=180&w=180&zc=1&q=100" alt="<?php the_title(); ?>" />
                </a>
            <?php endif; ?>
            <h2>
                <a href="<?php the_permalink(); ?>" title="<?php echo __('Permalink to ', 'duotive').the_title_attribute( 'echo=0' ); ?>" rel="bookmark">
                    <?php the_title(); ?>
                </a>
            </h2>                        
            <?php if(is_search() || is_tag() || has_excerpt( $post->ID ) ): ?>  
	            <?php the_excerpt(); ?>
				   <a class="more-link" href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php echo __('Read More &#187;','duotive'); ?></a>   
            <?php else: ?>
    	        <?php the_content(''); 		?>
	            <a class="more-link" href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php echo __('Read More &#187;','duotive'); ?></a>                
            <?php endif; ?>
            <div class="post-meta">

            </div>
        <!--end of blog-classic -->
        </div>
        </div>
<?php endwhile;?>
<?php if(function_exists('wp_pagenavi')): ?>
	<div id="navigation">
		<?php wp_pagenavi();?>  
	</div>                    
<?php endif; ?>