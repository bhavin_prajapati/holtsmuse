<?php
	/* SINGLE PAGES TEMPLATE */
	get_header();
?>
<?php if ( get_option('postsidebar') == '' ) $postsidebar = 'no'; else $postsidebar = get_option('postsidebar'); ?>
<div id="content-wrapper">
	
	<?php if(is_category('gallery') || in_category('gallery') || is_category('carousel') || in_category('carousel')){ ?>
	
		<div id="content" class="content-full <?php if ( $postsidebar == 'yes') echo 'content-full-width'; ?>">
	

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <div id="single">
				<?php if( is_category('video') || in_category('video')){
					include('single-video.php');
				}elseif( is_category('gallery') || in_category('gallery')){
					include('single-gallery.php');
				}elseif( is_category('carousel') || in_category('carousel')){
				include('single-carousel.php');
				}
        else{
					include('single-article.php');
				}
				?>
            </div>
      <div id="related-sep"></div>
			<div style="float:left; width:630px;">
			<?php
            	$relatedposts = get_option('relatedposts'); if ( $relatedposts == '' ) $relatedposts = 'off'; else $relatedposts = get_option('relatedposts');
				if ( $relatedposts != 'off' )
				{
					$relatedpostsnumber = get_option('relatedpostsnumber'); if ( $relatedpostsnumber == '' ) $relatedpostsnumber = 5; else $relatedpostsnumber = get_option('relatedpostsnumber');
					switch ( $relatedposts )
					{
						case 'category':
							$categories = get_the_category($post->ID);
							if ($categories) {
								$category_ids = array();
								foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
							
								$args=array(
									'category__in' => $category_ids,
									'post__not_in' => array($post->ID),
									'showposts'=> $relatedpostsnumber,
									'ignore_sticky_posts'=>1
								);
							}
						break;
						
						case 'tags':
							$tags = wp_get_post_tags($post->ID);
							if ($tags) {
								$tag_ids = array();
								foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
							
								$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'showposts'=> $relatedpostsnumber,
									'ignore_sticky_posts'=>1
								);
							}	
						break;							
					}
					if ( get_option('sharing') == '' ) $sharing = 'yes'; else $sharing = get_option('sharing');
					if ( $sharing == 'yes') require_once('includes/sharing.php');
						 	
					$my_query = new wp_query($args);
					if( $my_query->have_posts() )
					{
						echo '<div id="related" class="clearfix">';
							echo '<h3>Related Articles</h3>';
							echo '<ul>';
								$i = 1;
								while ($my_query->have_posts())
								{
									$my_query->the_post();
									?>
										<?php if ( has_post_thumbnail() ): ?>
											<li<?php if ( $i%5 == 0 && $postsidebar == 'no' ) echo ' class="last-related"'; ?>>
												<?php $thumbnail_src = wp_get_attachment_url( get_post_thumbnail_id($post->ID ) ); ?>
												<?php $website_url = get_bloginfo('wpurl'); ?>
												<?php $thumbnail_src = str_replace($website_url,'', $thumbnail_src); ?>                                          
												<a class="post-image" href="<?php the_permalink(); ?>" title="<?php the_title();?>">
													<img src="<?php echo get_bloginfo('template_directory');?>/includes/timthumb.php?src=<?php echo $thumbnail_src; ?>&amp;h=104&amp;w=104&amp;zc=1&amp;q=100" alt="<?php the_title(); ?>" />
												</a>
											</li>
										<?php endif; ?>                        
									<?php
									$i++;
								}
							echo '</ul>';
						echo '</div>';
					}
				}
            ?>   
		<?php wp_reset_query(); ?>          
        <?php if ( get_option('comments') == '' ) $comments = 'yes'; else $comments = get_option('comments'); ?>
        
        <?php if ( $comments == 'yes') comments_template( '', true ); ?>
        
		<?php endwhile; ?>
		</div>
		<?php if ( $postsidebar == 'no' ) : ?>
		
		<?php get_sidebar(); ?>
    <?php endif; ?>	
	<?php }
  
  else{ ?>
	<?php if ( $postsidebar == 'no' &&  !(is_category('carousel') || in_category('carousel')) ) : ?>
		<div class="side-push">
			<?php get_sidebar(); ?>
		</div>
    <?php endif; ?>
		<div id="content"<?php if ( $postsidebar == 'yes') echo ' class="content-full-width"'; ?>>

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <div id="single">
				<?php if( is_category('video') || in_category('video')){
					include('single-video.php');
				}elseif( is_category('gallery') || in_category('gallery')){
					include('single-gallery.php');
				}
        elseif( is_category('carousel') || in_category('carousel')){
					include('single-carousel.php');
				}
        else{
					include('single-article.php');
				}
				?>
            </div>
            <div id="related-sep"></div>
			<?php
            	$relatedposts = get_option('relatedposts'); if ( $relatedposts == '' ) $relatedposts = 'off'; else $relatedposts = get_option('relatedposts');
				if ( $relatedposts != 'off' )
				{
					$relatedpostsnumber = get_option('relatedpostsnumber'); if ( $relatedpostsnumber == '' ) $relatedpostsnumber = 5; else $relatedpostsnumber = get_option('relatedpostsnumber');
					switch ( $relatedposts )
					{
						case 'category':
							$categories = get_the_category($post->ID);
							if ($categories) {
								$category_ids = array();
								foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
							
								$args=array(
									'category__in' => $category_ids,
									'post__not_in' => array($post->ID),
									'showposts'=> $relatedpostsnumber,
									'ignore_sticky_posts'=>1
								);
							}
						break;
						
						case 'tags':
							$tags = wp_get_post_tags($post->ID);
							if ($tags) {
								$tag_ids = array();
								foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
							
								$args=array(
									'tag__in' => $tag_ids,
									'post__not_in' => array($post->ID),
									'showposts'=> $relatedpostsnumber,
									'ignore_sticky_posts'=>1
								);
							}	
						break;							
					}
					
					if ( get_option('sharing') == '' ) $sharing = 'yes'; else $sharing = get_option('sharing');		
					if ( $sharing == 'yes') require_once('includes/sharing.php');
					
					$my_query = new wp_query($args);
					if( $my_query->have_posts() )
					{
						echo '<div id="related" class="clearfix">';
							echo '<h3>Related</h3>';
							echo '<ul>';
								$i = 1;
								while ($my_query->have_posts())
								{
									$my_query->the_post();
									?>
										<?php if ( has_post_thumbnail() ): ?>
											<li<?php if ( $i%5 == 0 && $postsidebar == 'no' ) echo ' class="last-related"'; ?>>
												<?php $thumbnail_src = wp_get_attachment_url( get_post_thumbnail_id($post->ID ) ); ?>
												<?php $website_url = get_bloginfo('wpurl'); ?>
												<?php $thumbnail_src = str_replace($website_url,'', $thumbnail_src); ?>                                          
												<a class="post-image" href="<?php the_permalink(); ?>" title="<?php the_title();?>">
													<img src="<?php echo get_bloginfo('template_directory');?>/includes/timthumb.php?src=<?php echo $thumbnail_src; ?>&amp;h=104&amp;w=104&amp;zc=1&amp;q=100" alt="<?php the_title(); ?>" />
												</a>
											</li>
										<?php endif; ?>                        
									<?php
									$i++;
								}
							echo '</ul>';
						echo '</div>';
					}
				}
            ?>   
		<?php wp_reset_query(); ?>                    
        
        <?php if ( get_option('comments') == '' ) $comments = 'yes'; else $comments = get_option('comments'); ?>
        
        
        <?php if ( $comments == 'yes') comments_template( '', true ); ?>
        
		<?php endwhile; ?>
		
		<?php } ?>
		
    <!-- end of content -->
    </div>
<!--end of content wrapper -->    
</div>
<?php get_footer(); ?>
