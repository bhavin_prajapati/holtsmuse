<div id="gallery-page">
				<?php $slider_display = get_option('slider_display'); if ( $slider_display == '' ) $slider_display = '1'; else $slider_display = get_option('slider_display'); ?>
                <?php if ( $slider_display == 0 ) : ?>             
                    <h1 class="title">
                        <?php the_title(); ?>
                        <!--end of entry title -->
                    </h1>
                <?php endif; ?>
                <?php if ( get_option('postmeta') == '' ) $postmeta = 'yes'; else $postmeta = get_option('postmeta'); ?>
                <?php if ( $postmeta == 'yes' ): ?>
                    <!--div class="post-meta">
           
                        <span class="date">Posted on: <?php the_time('jS'); echo ' '; the_time('F'); echo ' '; the_time('Y');?></span>
                                            
                
                    </div-->    <!-- end of post meta -->
					<div class="related-sep"></div>
                <?php endif; ?>
                <?php if ( get_option('posttopimage') == '' ) $posttopimage = 'yes'; else $posttopimage = get_option('posttopimage'); ?>
                <?php if ( $posttopimage == 'yes' ): ?>        
					<div style="overflow:hidden; margin-bottom: 16px;">
<?php
				$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'orderby' => 'menu_order', 'order' => 'ASC' ); 
				$attachments = get_posts($args);
				$count = 1;
				if ($attachments) {
					if(count($attachments) == 1){
					 if ( has_post_thumbnail() )   :
						foreach ( $attachments as $attachment ) {
						$attc[] = wp_get_attachment_url( $attachment->ID );  ?>
					
                                 <span style="margin-left:15px"> 
                                                            
                                    <img src="<?php echo get_bloginfo('template_directory');?>/includes/timthumb.php?src=<?php echo $attc[0]; ?>&amp;h=450&amp;w=935&amp;zc=2&amp;q=100&cc=000000&amp;a=t" alt="<?php the_title(); ?>" />
                                </span>
                                                                              
                                     
                    <?php 	} ;
                     endif; 
				
					}else{
						foreach ( $attachments as $attachment ) {
							$pc = $attachment->post_content;
							$pe = $attachment->post_excerpt;
							$new = htmlspecialchars($pc, ENT_QUOTES);
							if( $count == count($attachments)){
								$attc[] = wp_get_attachment_url( $attachment->ID ) . '###'. $new . $pe . ' ';
							}else{
								$attc[] = wp_get_attachment_url( $attachment->ID ) . '###'. $new .'###' . $pe .' ,,, ';
							}
							$count++;
						}
							$comma_separated = implode("", $attc);
						//	echo   $comma_separated;
						echo do_shortcode('[slideshow effect="fold" width="935" height="450"]'.$comma_separated.'[/slideshow]');   
					//	echo do_shortcode('string mefager');  
					}	
				}
			?>   
			</div>
			
				<?php if($post->post_content != "") { ?>
					<div class="related-sep"></div>
                <?php } ?>
				<?php endif; ?>
                <div class="entry-content" style="clear:both">
                    <?php the_content('Read More'); ?>
                    <?php wp_link_pages( array( 'before' => '<span class="page-link">' . 'Pages:', 'after' => '</span>' ) ); ?>
                <!--end of entry content -->
				<p><?php the_tags(); ?></p>
                </div>
                <?php if ( get_the_author_meta( 'description' ) ) : ?>
                    <div id="author-info">
                        <div id="author-avatar">
							<?php $avatar_path = get_bloginfo('template_directory').'/images/default-avatar.png'; ?>
                            <?php echo get_avatar( get_the_author_meta( 'ID' ), $size = '74', $default = $avatar_path ); ?>
                        <!--end of author avatar -->
                        </div>
                        <div id="author-description">
                            <h6><?php echo __('About ', 'duotive').get_the_author(); ?></h6>
                            <p><?php the_author_meta( 'description' ); ?></p>
                            <a class="more-url" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                            	<?php echo __('View all posts','duotive'); ?>
                            </a>                            
                        <!-- end of author description -->
                        </div>
                    <!-- end of author info -->
                    </div>
                <?php endif; ?>  
            <!--end of post -->
</div>